#!/user/bin/env python
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'web2_project.settings')

import django
django.setup()

from rango.models import Category, Page


def populate():
    category = add_category('Python', 128, 64)

    add_page(cat=category,
        title="Official Python Tutorial",
        url="http://docs.python.org/2/tutorial/",
             views=200)

    add_page(cat=category,
        title="How to Think like a Computer Scientist",
        url="http://www.greenteapress.com/thinkpython/",
             views=100)

    add_page(cat=category,
        title="Learn Python in 10 Minutes",
        url="http://www.korokithakis.net/tutorials/python/",
             views=80)

    category = add_category(name="Django", views=64, likes=32)

    add_page(cat=category,
        title="Official Django Tutorial",
        url="https://docs.djangoproject.com/en/1.5/intro/tutorial01/",
             views=180)

    add_page(cat=category,
        title="Django Rocks",
        url="http://www.djangorocks.com/",
             views=70)

    add_page(cat=category,
        title="How to Tango with Django",
        url="http://www.tangowithdjango.com/",
             views=10)

    category = add_category(likes=16, views=32, name="Other Frameworks")

    add_page(cat=category,
        title="Bottle",
        url="http://bottlepy.org/docs/dev/",
             views=10)

    add_page(cat=category,
        title="Flask",
        url="http://flask.pocoo.org",
             views=78)

    category = add_category("Unified Modelling Language")

    # Print out what we have added to the user.
    for c in Category.objects.all():
        print "- {0}".format(str(c))
        for p in Page.objects.filter(category=c):
            print "-- {0}".format(str(p))

def add_page(cat, title, url, views=0):
    p = Page.objects.get_or_create(category=cat, title=title)[0]
    p.url=url
    p.views=views
    p.save()
    return p

def add_category(name, views=0, likes=0):
    c = Category.objects.get_or_create(name=name)[0]
    c.views=views
    c.likes=likes
    c.save()
    return c

# Start execution here!
if __name__ == '__main__':
    print "Starting Rango population script..."
    populate()