#coding: utf-8


from django.shortcuts import render


def about(request):
    context_dict = {}
    if request.session.get('visits'):
        visits = request.session.get('visits')
    else:
        visits = 0
    context_dict['visits'] = visits
    return render(request, 'rango/about.html', context_dict)

