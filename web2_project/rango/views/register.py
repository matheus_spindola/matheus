#coding: utf-8


from django.contrib.auth import logout
from django.shortcuts import render
from rango.forms import UserForm, UserProfileForm


def register(request):
    context_dict = {}
    if request.user:
        logout(request)

    registered = False
    if request.method != "POST":
        user_form = UserForm()
        profile_form = UserProfileForm()
    else:
        user_form = UserForm(request.POST)
        profile_form = UserProfileForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            profile.save()
            registered = True

    context_dict['registered'] = registered
    context_dict['user_form'] = user_form
    context_dict['profile_form'] = profile_form
    return render(request, 'rango/register.html', context_dict)