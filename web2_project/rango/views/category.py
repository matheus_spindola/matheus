#coding: utf-8

from django.shortcuts import render
from rango.models import Category, Page



def category(request, category_name_slug):
    context_dict = {}
    try:
        category = Category.objects.get(slug=category_name_slug)
        category.views += 1
        category.save()
        context_dict['category_name'] = category.name
        pages = Page.objects.filter(category=category).order_by('-views')
        context_dict['pages'] = pages
        context_dict['category'] = category
    except Category.DoesNotExist:
        pass

    #context_dict['category_name'] = category_name_slug
    #context_dict['category'] = category_name_slug
    #print(category_name_slug)
    #return(HttpResponse(category_name_slug))
    return render(request, 'rango/category.html', context_dict)

