#coding: utf-8

from index import *
from rango.forms import CategoryForm
from django.views.generic import *
from LoginRequired import *




class Add_Category(LoginRequiredMixin, View):
    template_name = 'rango/add_category.html'

    def get(self,request):
        context_dict = {}
        form = CategoryForm
        context_dict['form'] = form
        return render(request, self.template_name , context_dict)

    def post(self, request):

        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return index(request)
        else:
            print form.errors