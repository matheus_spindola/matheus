#coding: utf-8

#from datetime import datetime
#from django.contrib.auth import authenticate, login, logout
#from django.contrib.auth.decorators import login_required
#from django.http import HttpResponse, HttpResponseRedirect
#from django.shortcuts import render, redirect
#from models import Category, Page, User, UserProfile
#from forms import CategoryForm, PageForm, UserForm, UserProfileForm


from about import*
from add_category import*
from add_page import*
from category import*
from get_category_list import*
from index import*
from like_category import*
from register import*
from restricted import*
from suggest_category import*
from track_url import*
from user_login import*
from user_profile import*
from users_list import*
