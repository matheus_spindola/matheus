#coding: utf-8


from django.shortcuts import render
from get_category_list import*



def suggest_category(request):

    context_dict = {}
    starts_with = ''
    if request.method == 'GET':
        starts_with = request.GET['suggestion']

    cats = get_category_list(8, starts_with)
    context_dict['cats'] = cats
    return render(request, 'rango/cats.html', context_dict)

# def add_category(request):
#     context_dict = {}
#
#     return render(request, 'rango/add_category.html', context_dict)
