#coding: utf-8


from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render



def user_login(request):
    context_dict = {}
    if request.method != "POST":
        return render(request, 'rango/login.html', context_dict)
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/rango/')
            else:
                return HttpResponse("Sua conta está desabilitada.")
        else:
            erro = "Detalhes de login inválidos: Usuário: [{0}], Senha:[{1}]".format(username, password)
            print(erro)
            return HttpResponse(erro)
