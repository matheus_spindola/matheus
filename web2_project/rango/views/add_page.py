#coding: utf-8


from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from rango.models import Category
from rango.forms import PageForm
from category import*



@login_required
def add_page(request, category_name_slug):
    try:
        cat = Category.objects.get(slug=category_name_slug)
    except Category.DoesNotExist:
        cat = None

    if request.method != 'POST':
        form = PageForm()
    else:
        form = PageForm(request.POST)
        if form.is_valid():
            if cat:
                page = form.save(commit=False)
                page.category = cat
                page.views = 0
                page.save()
                return category(request, category_name_slug)
        else:
            print form.errors

    context_dict = {'form':form, 'category': cat}
    return render(request, 'rango/add_page.html', context_dict)
