from django import forms
from rango.models import Page


class PageForm(forms.ModelForm):
    title = forms.CharField(max_length=128, help_text="Informe o titulo da pagina.")
    url = forms.URLField(max_length=200, help_text="Informe a URL da pagina.")
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

    def clean(self):
        cleaned_data = self.cleaned_data
        url = cleaned_data.get('url')

        if url and not url.startswith('http://'):
            url = 'http://' + url
            cleaned_data['url'] = url

        return cleaned_data

    class Meta:
        model = Page
        exclude = ('category',)
