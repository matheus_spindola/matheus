from django.contrib import admin
from rango.models import Category, Page, UserProfile

from CategoryAdmin import *
from PageAdmin import *


admin.site.register(Category, CategoryAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(UserProfile)