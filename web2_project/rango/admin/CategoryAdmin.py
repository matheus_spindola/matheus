from django.contrib import admin


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name','views','likes')
    prepopulated_fields = {'slug':('name',)}



