# README #

## Tutorial WEB-2 ##

## Link do tutorial ##

* http://www.tangowithdjango.com/

### Configurações para fazer tutorial ###

* Instalar git, pip, virtualenv e virtualenvwrapper

### Configurando git ###

* git config --global user.name "matheus"
* git config --global user.email matheus@gmail.com
* git config --global core.editor nano
* git config --global credential.helper 'cache --timeout=3600'

### Criar repositório ###

* mkdir local
* cd local
* git init
* git remote add origin https://matheus_spindola@bitbucket.org/matheus_spindola/matheus.git
* git add contributors.txt
* git commit -m "Initial commit with contributors"
* git push -u origin master

### Pegar o que esta na nuvem ###

* git pull

### Intalação do pip ###

* sudo apt-get install python-pip python3-pip (pacotes relacionados ao python)

### Intalação do virtualenv ###

* pip install virtualenv virtualenvwrapper
* echo "source virtualenvwrapper.sh" >> ~/.bashrc"

### Criar máquina virtual ###

* virtualenv web2_projec
* mkdir .virtualenvs
* cd .virtualenvs/
* virtualenv rango
* cd rango
* cd bin
* source ~/.virtualenvs/rango/bin/activate (workon - entra no projeto)
* cd web2_project
* pip install -r requirements.txt

### Criando a máquina virtual ###

* mkvirtualenv web2_project
* deactivate ( desativa )
* workon projeto ( ativa )
* pip list
* pip freeze

### Intalação pillow ###

* pip install pillow ( gerenciamento de imagens )

### Colocar python no ar ###

* python manage.py runserver

### Mover pacote ###

* sudo mv home/alono/downloads/nome

### Descompactação tar.gz ###

* tar -xzvf pucharm-profissional-4.5.3.tar.gz

### slugify ###
* padroniza a URL

### Criar banco ###

* workon web2_project
* python manage.py migrate
* python manage.py createsuperuser
* python populate_rango.py